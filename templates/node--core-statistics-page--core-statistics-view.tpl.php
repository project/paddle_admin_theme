<?php

/**
 * @file
 * Template that displays the core statistics view version of a core statistics.
 */
?>
<a href="<?php print $output['link']; ?>">
  <div class="node-core-statistics-view">
    <div class="node-core-statistics-view-image">
      <?php print $output['image']; ?>
    </div>
    <div class="node-content">
      <div class="node-header">
        <?php if (!empty($publication_date)): ?>
          <h5>
            <?php print $publication_date; ?>
          </h5>
        <?php endif; ?>
        <h4>
          <?php print $output['title']; ?>
        </h4>
      </div>
    </div>
  </div>
</a>

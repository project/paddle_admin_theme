var themeTableHeaderOffset = function() {
  var offsetheight = jQuery("#header").height();
  return offsetheight;
};

(function ($) {
  Drupal.behaviors.paddle_chronological_event_date = {
    attach: function (context, settings) {
      var form_id = '#paddle-panes-listing-content-type-edit-form';

      // Hide the Chronological by event date by default.
      $(form_id + ' #edit-sorting-type #edit-sorting-type-event-sort-asc').parent().hide();

      // If the Event page is already checked, Display the Chronological by event date option.
      if ($(form_id + ' #edit-content-types-event-page').is(':checked')) {
        $(form_id + ' #edit-sorting-type #edit-sorting-type-event-sort-asc').parent().show();
      }

      // Handle the display of Chronological by event date option based on the Event page selection.
      $(form_id + ' #edit-content-types-event-page').change(function () {
        $(form_id + ' #edit-sorting-type #edit-sorting-type-event-sort-asc').parent().toggle();
        $(form_id + ' #edit-sorting-type #edit-sorting-type-event-sort-asc').attr('checked', false);
      });
    }
  }
})(jQuery);
